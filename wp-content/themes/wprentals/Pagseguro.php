<?php

require_once dirname(__FILE__).'/PagSeguro/PagSeguroLibrary.php';

class Pagseguro {

    public $email = "lucas.tec22@hotmail.com";
    public $token = "3607EEFC46844B67AEE444FDA16F1F8C";
    public $ambiente = "sandbox";
    public $sessionId;

    public function __construct(){

        if(!defined('CONFIG_AMBIENTE')):
            define('CONFIG_AMBIENTE', $this->ambiente);

        elseif(!defined('CONFIG_EMAIL')):
            define('CONFIG_EMAIL', $this->email);

        elseif(!defined('CONFIG_TOKEN')):
            define('CONFIG_TOKEN', $this->token);
        endif;

        $this->sessionId();
    }

    public function sessionId(){

        try {
            $credentials = new PagSeguroAccountCredentials($this->email, $this->token);
            $session = PagSeguroSessionService::getSession($credentials);

            $this->sessionId = $session;
            return $session;

        } catch (PagSeguroServiceException $e) {
            die($e->getMessage());
        }
    }

    public function debitoDireto($post){

        $directPaymentRequest = new PagSeguroDirectPaymentRequest();
        $directPaymentRequest->setPaymentMode('DEFAULT');
        $directPaymentRequest->setPaymentMethod('EFT');
        $directPaymentRequest->setReceiverEmail($this->email);
        $directPaymentRequest->setCurrency("BRL");

        // Add an item for this payment request
        $directPaymentRequest->addItem(
            '1',
            $post->item_descricao,
            1,
            $post->total
        );

        $directPaymentRequest->setReference("REF123");

        $directPaymentRequest->setSender(
            $post->nome_comprador,
            $post->email_comprador,
            $post->ddd_comprador,
            $post->tel_comprador,
            'CPF',
            $post->cpf_comprador,
            true
        );
        $directPaymentRequest->setSenderHash($post->senderHash);

        // Set shipping information for this payment request
        $sedexCode = 3;
        $directPaymentRequest->setShippingType($sedexCode);
        $directPaymentRequest->setShippingAddress(
            '01452002',
            'Av. Brig. Faria Lima',
            '1384',
            'apto. 114',
            'Jardim Paulistano',
            'São Paulo',
            'SP',
            'BRA'
        );

        // Set bank for this payment request
        $directPaymentRequest->setOnlineDebit(
            array(
                "bankName" => $post->pag_banco
            )
        );

        try {

            $credentials = new PagSeguroAccountCredentials($this->email, $this->token);
            // Register this payment request in PagSeguro to obtain the payment URL to redirect your customer.
            $return = $directPaymentRequest->register($credentials);

            self::retornoTransacao($return, $post->forma_pagamento);

        } catch (PagSeguroServiceException $e) {
            die($e->getMessage());
        }
    }

    public static function retornoDebito($transaction){
        if ($transaction) {
            echo "<h2>Retorno da transa&ccedil;&atilde;o de D&eacute;bito Online</h2>";
            echo "<p><strong>Date: </strong> ".$transaction->getDate() ."</p> ";
            echo "<p><strong>lastEventDate: </strong> ".$transaction->getLastEventDate()."</p> ";
            echo "<p><strong>code: </strong> ".$transaction->getCode() ."</p> ";
            echo "<p><strong>reference: </strong> ".$transaction->getReference() ."</p> ";
            echo "<p><strong>recovery code: </strong> ".$transaction->getRecoveryCode() ."</p> ";
            echo "<p><strong>type: </strong> ".$transaction->getType()->getValue() ."</p> ";
            echo "<p><strong>status: </strong> ".$transaction->getStatus()->getValue() ."</p> ";
            echo "<p><strong>paymentLink: </strong> ".$transaction->getPaymentLink()."</p>";
            echo "<p><strong>paymentMethodType: </strong> ".$transaction->getPaymentMethod()->getType()->getValue() ."</p> ";
            echo "<p><strong>paymentModeCode: </strong> ".$transaction->getPaymentMethod()->getCode()->getValue() ."</p> ";
            echo "<p><strong>grossAmount: </strong> ".$transaction->getGrossAmount() ."</p> ";
            echo "<p><strong>discountAmount: </strong> ".$transaction->getDiscountAmount() ."</p> ";
            echo "<p><strong>feeAmount: </strong> ".$transaction->getFeeAmount() ."</p> ";
            echo "<p><strong>netAmount: </strong> ".$transaction->getNetAmount() ."</p> ";
            echo "<p><strong>extraAmount: </strong> ".$transaction->getExtraAmount() ."</p> ";
            echo "<p><strong>installmentCount: </strong> ".$transaction->getInstallmentCount() ."</p> ";
            echo "<p><strong>itemCount: </strong> ".$transaction->getItemCount() ."</p> ";
            echo "<p><strong>Items: </strong></p>";
            foreach ($transaction->getItems() as $item)
            {
                echo "<p><strong>id: </strong> ". $item->getId() ."</br> ";
                echo "<strong>description: </strong> ". $item->getDescription() ."</br> ";
                echo "<strong>quantity: </strong> ". $item->getQuantity() ."</br> ";
                echo "<strong>amount: </strong> ". $item->getAmount() ."</p> ";
            }
            echo "<p><strong>senderName: </strong> ".$transaction->getSender()->getName() ."</p> ";
            echo "<p><strong>senderEmail: </strong> ".$transaction->getSender()->getEmail() ."</p> ";
            echo "<p><strong>senderPhone: </strong> ".$transaction->getSender()->getPhone()->getAreaCode() . " - " .
                 $transaction->getSender()->getPhone()->getNumber() . "</p> ";
            echo "<p><strong>Shipping: </strong></p>";
            echo "<p><strong>street: </strong> ".$transaction->getShipping()->getAddress()->getStreet() ."</p> ";
            echo "<p><strong>number: </strong> ".$transaction->getShipping()->getAddress()->getNumber()  ."</p> ";
            echo "<p><strong>complement: </strong> ".$transaction->getShipping()->getAddress()->getComplement()  ."</p> ";
            echo "<p><strong>district: </strong> ".$transaction->getShipping()->getAddress()->getDistrict()  ."</p> ";
            echo "<p><strong>postalCode: </strong> ".$transaction->getShipping()->getAddress()->getPostalCode()  ."</p> ";
            echo "<p><strong>city: </strong> ".$transaction->getShipping()->getAddress()->getCity()  ."</p> ";
            echo "<p><strong>state: </strong> ".$transaction->getShipping()->getAddress()->getState()  ."</p> ";
            echo "<p><strong>country: </strong> ".$transaction->getShipping()->getAddress()->getCountry()  ."</p> ";
        }
      echo "<pre>";
    }

    public function boletoDireto($post){

        $directPaymentRequest = new PagSeguroDirectPaymentRequest();
        $directPaymentRequest->setPaymentMode('DEFAULT');
        $directPaymentRequest->setPaymentMethod('BOLETO');
        $directPaymentRequest->setReceiverEmail($this->email);
        $directPaymentRequest->setCurrency("BRL");

        // Add an item for this payment request
        $directPaymentRequest->addItem(
            '1',
            $post->item_descricao,
            1,
            $post->total
        );

        $directPaymentRequest->setReference("REF123");

        $directPaymentRequest->setSender(
            $post->nome_comprador,
            $post->email_comprador,
            $post->ddd_comprador,
            $post->tel_comprador,
            'CPF',
            $post->cpf_comprador,
            true
        );
        $directPaymentRequest->setSenderHash($post->senderHash);

        // Set shipping information for this payment request
        $sedexCode = 3;
        $directPaymentRequest->setShippingType($sedexCode);
        $directPaymentRequest->setShippingAddress(
            '01452002',
            'Av. Brig. Faria Lima',
            '1384',
            'apto. 114',
            'Jardim Paulistano',
            'São Paulo',
            'SP',
            'BRA'
        );

        try {

            $credentials = new PagSeguroAccountCredentials($this->email, $this->token);
            // Register this payment request in PagSeguro to obtain the payment URL to redirect your customer.
            $return = $directPaymentRequest->register($credentials);

            self::retornoTransacao($return, $post->forma_pagamento);

        } catch (PagSeguroServiceException $e) {
            die($e->getMessage());
        }
    }

    public static function retornoTransacao($transaction, $forma_pagamento){
        if ($transaction) {

            $json = array(
                'forma_pagamento' => $forma_pagamento,
                'code' => $transaction->getCode(),
                'status' => $transaction->getStatus()->getValue(),
                'paymentLink' => $transaction->getPaymentLink(),
            );

            echo json_encode($json);
        }
    }

    public function cartaoDireto($post){
        // Instantiate a new payment request
        $directPaymentRequest = new PagSeguroDirectPaymentRequest();
        // Set the Payment Mode for this payment request
        $directPaymentRequest->setPaymentMode('DEFAULT');
        // Set the Payment Method for this payment request
        $directPaymentRequest->setPaymentMethod('CREDIT_CARD');
        /**
        * @todo Change the receiver Email
        */
        $directPaymentRequest->setReceiverEmail($this->email);
        // Set the currency
        $directPaymentRequest->setCurrency("BRL");
        // Add an item for this payment request
        // Add an item for this payment request
        $directPaymentRequest->addItem(
            '1',
            $post->item_descricao,
            1,
            $post->total
        );

        // Set a reference code for this payment request. It is useful to identify this payment
        // in future notifications.
        $directPaymentRequest->setReference("REF123");
        // Set your customer information.
        // If you using SANDBOX you must use an email @sandbox.pagseguro.com.br
        $directPaymentRequest->setSender(
            $post->nome_comprador,
            $post->email_comprador,
            $post->ddd_comprador,
            $post->tel_comprador,
            'CPF',
            $post->cpf_comprador,
            true
        );
        $directPaymentRequest->setSenderHash($post->senderHash);
        // Set shipping information for this payment request
        
        $sedexCode = 3;
        $directPaymentRequest->setShippingType($sedexCode);
        $directPaymentRequest->setShippingAddress(
            '01452002',
            'Av. Brig. Faria Lima',
            '1384',
            'apto. 114',
            'Jardim Paulistano',
            'São Paulo',
            'SP',
            'BRA'
        );
        //Set billing information for credit card
        $billing = new PagSeguroBilling
        (
            array(
                'postalCode' => '01452002',
                'street' => 'Av. Brig. Faria Lima',
                'number' => '1384',
                'complement' => 'apto. 114',
                'district' => 'Jardim Paulistano',
                'city' => 'São Paulo',
                'state' => 'SP',
                'country' => 'BRA'
            )
        );
        
        $token = $post->cardToken;
        $installment = new PagSeguroDirectPaymentInstallment(
            array(
              "quantity" => 1,
              "value" => $post->total,
              "noInterestInstallmentQuantity" => 2
            )
        );
        $cardCheckout = new PagSeguroCreditCardCheckout(
            array(
                'token' => $token,
                'installment' => $installment,
                'holder' => new PagSeguroCreditCardHolder(
                    array(
                        'name' => $post->nome_comprador, //Equals in Credit Card
                        'documents' => array(
                            'type' => 'CPF',
                            'value' => $post->cpf_comprador
                        ),
                        'birthDate' => date($post->nascimento_comprador),
                        'areaCode' => $post->ddd_comprador,
                        'number' => $post->tel_comprador
                    )
                ),
                'billing' => $billing
            )
        );
        //Set credit card for payment
        $directPaymentRequest->setCreditCard($cardCheckout);
        try {
            /**
             * #### Credentials #####
             * Replace the parameters below with your credentials
             * You can also get your credentials from a config file. See an example:
             * $credentials = PagSeguroConfig::getAccountCredentials();
             */
            // seller authentication
            //$credentials = new PagSeguroAccountCredentials("vendedor@lojamodelo.com.br",
            //    "E231B2C9BCC8474DA2E260B6C8CF60D3");
            // application authentication
            $credentials = new PagSeguroAccountCredentials($this->email, $this->token);
            // Register this payment request in PagSeguro to obtain the payment URL to redirect your customer.
            $return = $directPaymentRequest->register($credentials);
            self::retornoTransacao($return, $post->forma_pagamento);
        } catch (PagSeguroServiceException $e) {
            die($e->getMessage());
        }
    }

    public static function retornoCartao($transaction){
        if ($transaction) {
            echo "<h2>Retorno da transa&ccedil;&atilde;o com Cart&atilde;o de Cr&eacute;dito.</h2>";
            echo "<p><strong>Date: </strong> ".$transaction->getDate() ."</p> ";
            echo "<p><strong>lastEventDate: </strong> ".$transaction->getLastEventDate()."</p> ";
            echo "<p><strong>code: </strong> ".$transaction->getCode() ."</p> ";
            echo "<p><strong>reference: </strong> ".$transaction->getReference() ."</p> ";
            echo "<p><strong>type: </strong> ".$transaction->getType()->getValue() ."</p> ";
            echo "<p><strong>status: </strong> ".$transaction->getStatus()->getValue() ."</p> ";
            echo "<p><strong>paymentMethodType: </strong> ".$transaction->getPaymentMethod()->getType()->getValue() ."</p> ";
            echo "<p><strong>paymentModeCode: </strong> ".$transaction->getPaymentMethod()->getCode()->getValue() ."</p> ";
            echo "<p><strong>grossAmount: </strong> ".$transaction->getGrossAmount() ."</p> ";
            echo "<p><strong>discountAmount: </strong> ".$transaction->getDiscountAmount() ."</p> ";
            echo "<p><strong>feeAmount: </strong> ".$transaction->getFeeAmount() ."</p> ";
            echo "<p><strong>netAmount: </strong> ".$transaction->getNetAmount() ."</p> ";
            echo "<p><strong>extraAmount: </strong> ".$transaction->getExtraAmount() ."</p> ";
            echo "<p><strong>installmentCount: </strong> ".$transaction->getInstallmentCount() ."</p> ";
            echo "<p><strong>itemCount: </strong> ".$transaction->getItemCount() ."</p> ";
            echo "<p><strong>Items: </strong></p>";
            foreach ($transaction->getItems() as $item)
            {
                echo "<p><strong>id: </strong> ". $item->getId() ."</br> ";
                echo "<strong>description: </strong> ". $item->getDescription() ."</br> ";
                echo "<strong>quantity: </strong> ". $item->getQuantity() ."</br> ";
                echo "<strong>amount: </strong> ". $item->getAmount() ."</p> ";
            }
            echo "<p><strong>senderName: </strong> ".$transaction->getSender()->getName() ."</p> ";
            echo "<p><strong>senderEmail: </strong> ".$transaction->getSender()->getEmail() ."</p> ";
            echo "<p><strong>senderPhone: </strong> ".$transaction->getSender()->getPhone()->getAreaCode() . " - " .
                 $transaction->getSender()->getPhone()->getNumber() . "</p> ";
            echo "<p><strong>Shipping: </strong></p>";
            echo "<p><strong>street: </strong> ".$transaction->getShipping()->getAddress()->getStreet() ."</p> ";
            echo "<p><strong>number: </strong> ".$transaction->getShipping()->getAddress()->getNumber()  ."</p> ";
            echo "<p><strong>complement: </strong> ".$transaction->getShipping()->getAddress()->getComplement()  ."</p> ";
            echo "<p><strong>district: </strong> ".$transaction->getShipping()->getAddress()->getDistrict()  ."</p> ";
            echo "<p><strong>postalCode: </strong> ".$transaction->getShipping()->getAddress()->getPostalCode()  ."</p> ";
            echo "<p><strong>city: </strong> ".$transaction->getShipping()->getAddress()->getCity()  ."</p> ";
            echo "<p><strong>state: </strong> ".$transaction->getShipping()->getAddress()->getState()  ."</p> ";
            echo "<p><strong>country: </strong> ".$transaction->getShipping()->getAddress()->getCountry()  ."</p> ";
        }else{
            return null;
        }
    }

    

    public function pagamentoLightbox(){

        echo "Chama a Função do Lightbox";
        /*
        // Instantiate a new payment request
        $paymentRequest = new PagSeguroPaymentRequest();
        $paymentRequest->setCurrency("BRL");

        // Add an item for this payment request
        $paymentRequest->addItem('0001', 'Notebook prata', 2, 430.00);
        $paymentRequest->setReference("REF123");

        // Set shipping information for this payment request
        $sedexCode = PagSeguroShippingType::getCodeByType('SEDEX');
        $paymentRequest->setShippingType($sedexCode);
        $paymentRequest->setShippingAddress(
            '01452002',
            'Av. Brig. Faria Lima',
            '1384',
            'apto. 114',
            'Jardim Paulistano',
            'São Paulo',
            'SP',
            'BRA'
        );

        // Set your customer information.
        $paymentRequest->setSender(
            'João Comprador',
            'email@comprador.com.br',
            '11',
            '56273440',
            'CPF',
            '156.009.442-76'
        );

        // Set the url used by PagSeguro to redirect user after checkout process ends
        $paymentRequest->setRedirectUrl("http://www.lojamodelo.com.br");

        // Add checkout metadata information
        $paymentRequest->addMetadata('PASSENGER_CPF', '15600944276', 1);

        // Another way to set checkout parameters
        $paymentRequest->addParameter('notificationURL', 'http://www.lojamodelo.com.br/nas');
        $paymentRequest->addParameter('senderBornDate', '07/05/1981');

        try {

            // seller authentication
            $credentials = new PagSeguroAccountCredentials("vendedor@lojamodelo.com.br",
                "E231B2C9BCC8474DA2E260B6C8CF60D3");

            // application authentication
            //$credentials = PagSeguroConfig::getApplicationCredentials();

            //$credentials->setAuthorizationCode("E231B2C9BCC8474DA2E260B6C8CF60D3");

            // Register this payment request in PagSeguro to obtain the checkout code
            $onlyCheckoutCode = true;
            $code = $paymentRequest->register($credentials, $onlyCheckoutCode);

            self::printPaymentUrl($code);
        } catch (PagSeguroServiceException $e) {
            die($e->getMessage());
        }
        */
    }

    public function pagamentoPagina($post){
        
        $paymentRequest = new PagSeguroPaymentRequest();
        $paymentRequest->setCurrency("BRL");

        $paymentRequest->addItem(
            $post['faturaID'],
            $post['Descricao'],
            1,
            floatval($post['Valor'])
        );

        $paymentRequest->setReference("FAT".$post['faturaID']);

        // Set shipping information for this payment request
        $sedexCode = PagSeguroShippingType::getCodeByType('SEDEX');
        $paymentRequest->setShippingType($sedexCode);
        $paymentRequest->setShippingAddress(
            $post['clienteCEP'],
            $post['clienteLogradouro'],
            $post['clienteNumero'],
            $post['clienteComplemento'],
            $post['clienteBairro'],
            $post['clienteCidade'],
            $post['clienteUF'],
            'BRA'
        );

        // Set your customer information.
        if($post['clienteCPF'] != ""){
            $label_cpf_cnpj = 'CPF';
            $cpf_cnpj = $post['clienteCPF'];
        }else{
            $label_cpf_cnpj = 'CNPJ';
            $cpf_cnpj = $post['clienteCNPJ'];
        }

        $paymentRequest->setSender(
            $post['clienteNome'],
            $post['clienteEmail'],
            $post['clienteDDD'],
            $post['clienteTel'],
            $label_cpf_cnpj,
            $cpf_cnpj,
            true
        );

        // Set the url used by PagSeguro to redirect user after checkout process ends
        $paymentRequest->setRedirectUrl("http://www.lojamodelo.com.br");

        // Another way to set checkout parameters
        $paymentRequest->addParameter('notificationURL', 'http://www.lojamodelo.com.br/nas');
        #$paymentRequest->addParameter('senderBornDate', '07/05/1981');

        // Add installment without addition per payment method
        //$paymentRequest->addPaymentMethodConfig('CREDIT_CARD', 6, 'MAX_INSTALLMENTS_NO_INTEREST');

        // Add installment limit per payment method
        //$paymentRequest->addPaymentMethodConfig('CREDIT_CARD', 8, 'MAX_INSTALLMENTS_LIMIT');

        // Add and remove a group and payment methods
        //$paymentRequest->acceptPaymentMethodGroup('CREDIT_CARD', 'ONLINE_DEBIT');      
        //$paymentRequest->excludePaymentMethodGroup('BOLETO', 'BOLETO');

        try {
            $credentials = new PagSeguroAccountCredentials(CONFIG_EMAIL, CONFIG_TOKEN);
            $url = $paymentRequest->register($credentials);

            echo $url;

        } catch (PagSeguroServiceException $e) {
            die($e->getMessage());
        }
    }    
}

?>