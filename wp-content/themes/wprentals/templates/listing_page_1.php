<?php

global $wpdb;
global $post;
global $current_user;
global $feature_list_array;
global $propid ;
global $post_attachments;
global $options;
global $where_currency;
global $property_description_text;     
global $property_details_text;
global $property_features_text;
global $property_adr_text;  
global $property_price_text;   
global $property_pictures_text;    
global $propid;
global $gmap_lat;  
global $gmap_long;
global $unit;
global $currency;
global $use_floor_plans;
global $favorite_text;
global $favorite_class;
global $property_action_terms_icon;
global $property_action;
global $property_category_terms_icon;
global $property_category;
global $guests;
global $bedrooms;
global $bathrooms;
global $show_sim_two;

//echo 'gorilla glass'; die();

$price              =   intval   ( get_post_meta($post->ID, 'property_price', true) );

$price_label        =   esc_html ( get_post_meta($post->ID, 'property_label', true) );  

$property_city      =   get_the_term_list($post->ID, 'property_city', '', ', ', '') ;

$property_area      =   get_the_term_list($post->ID, 'property_area', '', ', ', '');



$post_id=$post->ID; 

$guest_list= wpestate_get_guest_dropdown();

?>





   



<div class="row content-fixed-listing listing_type_1">

    <?php //get_template_part('templates/breadcrumbs'); ?>

    <div class=" <?php 

    if ( $options['content_class']=='col-md-12' || $options['content_class']=='none'){

        print 'col-md-8';

    }else{

       print  $options['content_class']; 

    }?> ">

    

        <?php get_template_part('templates/ajax_container'); ?>

        <?php

        while (have_posts()) : the_post();

            $image_id       =   get_post_thumbnail_id();

            $image_url      =   wp_get_attachment_image_src($image_id, 'wpestate_property_full_map');

            $full_img       =   wp_get_attachment_image_src($image_id, 'full');

            $image_url      =   $image_url[0];

            $full_img       =   $full_img [0];     

        ?>

        

     

    <div class="single-content listing-content">

        <h1 class="entry-title entry-prop"><?php the_title(); ?>        

            <span class="property_ratings">

                <?php 

                $args = array(

                    'number' => '15',

                    'post_id' => $post->ID, // use post_id, not post_ID

                );

                $comments   =   get_comments($args);

                $coments_no =   0;

                $stars_total=   0;



                foreach($comments as $comment) :

                    $coments_no++;

                    $rating= get_comment_meta( $comment->comment_ID , 'review_stars', true );

                    $stars_total+=$rating;

                endforeach;



                if($stars_total!= 0 && $coments_no!=0){

                    $list_rating= ceil($stars_total/$coments_no);

                    $counter=0; 

                    while($counter<5){

                        $counter++;

                        if( $counter<=$list_rating ){

                            print '<i class="fa fa-star"></i>';

                        }else{

                            print '<i class="fa fa-star-o"></i>'; 

                        }



                    }

                    print '<span class="rating_no">('.$coments_no.')</span>';

                }  

                ?>         

            </span> 

        </h1>

       

        <div class="listing_main_image_location">

            <?php print  $property_city.', '.$property_area; ?>        

        </div>   





        <div class="panel-wrapper imagebody_wrapper">

            <div class="panel-body imagebody imagebody_new">

                <?php  

                get_template_part('templates/property_pictures3');

                ?>

            </div> 

        </div>



        

        

        <div class="category_wrapper ">

            <div class="category_details_wrapper">

                <?php if( $property_action!='') {

                    echo $property_action; ?> <span class="property_header_separator">|</span>

                <?php } ?>

                

                <?php  if( $property_category!='') {

                    echo $property_category;?> <span class="property_header_separator">|</span> 

                <?php } ?> 

                    

                <?php print '<span class="no_link_details">'.$guests.' '. esc_html__( 'Guests','wpestate').'</span>';?> <span class="property_header_separator">|</span>

                <?php print '<span class="no_link_details">'.$bedrooms.' '.esc_html__( 'Bedrooms','wpestate').'</span>';?><span class="property_header_separator">|</span>

                <?php print '<span class="no_link_details">'.$bathrooms.' '.esc_html__( 'Baths','wpestate').'</span>';?>

            </div>



            <a href="#listing_calendar" class="check_avalability"><?php esc_html_e('Check Availability','wpestate');?></a>

        </div>

        

        

        

        <div id="listing_description">

        <?php

            $content = get_the_content();

            $content = apply_filters('the_content', $content);

            $content = str_replace(']]>', ']]&gt;', $content);

            if($content!=''){   

                print '<h4 class="panel-title-description">'.esc_html__('Description','wpestate').'</h4>';

                print '<div class="panel-body">'.$content.'</div>';       

            }

        ?>

        </div>

        <div id="view_more_desc"><?php esc_html_e('View more','wpestate');?></div>

     

          

        <!-- property price   -->   

        <div class="panel-wrapper" id="listing_price">

            <a class="panel-title" data-toggle="collapse" data-parent="#accordion_prop_addr" href="#collapseOne"> <span class="panel-title-arrow"></span>

                <?php if($property_price_text!=''){

                    echo $property_price_text;

                } else{

                    esc_html_e('Property Price','wpestate');

                }  ?>

            </a>

            <div id="collapseOne" class="panel-collapse collapse in">

                <div class="panel-body panel-body-border">

                    <?php print estate_listing_price($post->ID); ?>

                    <?php  wpestate_show_custom_details($post->ID); ?>

                </div>

            </div>

        </div>

        

        

        

        <!--campos endereço retirados daqui -->
      

        

        

        <!-- property details   -->  

        <div class="panel-wrapper">

            <?php                                       

            if($property_details_text=='') {

                print'<a class="panel-title" id="listing_details" data-toggle="collapse" data-parent="#accordion_prop_addr" href="#collapseTree"><span class="panel-title-arrow"></span>'.esc_html__( 'Property Details', 'wpestate').'  </a>';

            }else{

                print'<a class="panel-title"  id="listing_details" data-toggle="collapse" data-parent="#accordion_prop_addr" href="#collapseTree"><span class="panel-title-arrow"></span>'.$property_details_text.'  </a>';

            }

            ?>

            <div id="collapseTree" class="panel-collapse collapse in">

                <div class="panel-body panel-body-border">

                    <?php print estate_listing_details($post->ID);?>

                </div>

            </div>

        </div>



        



        <!-- Features and Amenities -->

        <div class="panel-wrapper features_wrapper">

            <?php 



            if ( count( $feature_list_array )!=0 && !count( $feature_list_array )!=1 ){ //  if are features and ammenties

                if($property_features_text ==''){

                    print '<a class="panel-title" id="listing_ammenities" data-toggle="collapse" data-parent="#accordion_prop_addr" href="#collapseFour"><span class="panel-title-arrow"></span>'.esc_html__( 'Amenities and Features', 'wpestate').'</a>';

                }else{

                    print '<a class="panel-title" id="listing_ammenities" data-toggle="collapse" data-parent="#accordion_prop_addr" href="#collapseFour"><span class="panel-title-arrow"></span>'. $property_features_text.'</a>';

                }

                ?>

                <div id="collapseFour" class="panel-collapse collapse in">

                    <div class="panel-body panel-body-border">

                        <?php print estate_listing_features($post->ID); ?>

                    </div>

                </div>

            <?php

            } // end if are features and ammenties

            ?>

        </div>

        

         

       

        <div class="property_page_container ">

            <?php

            get_template_part ('/templates/show_avalability');

            wp_reset_query();

            ?>  

        </div>    

         
         <!-- trazidos para cá -->
         
         <div class="panel-wrapper">

            <!-- property address   -->             

            <a class="panel-title" data-toggle="collapse" data-parent="#accordion_prop_addr" href="#collapseTwo">  <span class="panel-title-arrow"></span>

                <?php if($property_adr_text!=''){

                    echo $property_adr_text;

                } else{

                    esc_html_e('Property Address','wpestate');

                }

                ?>

            </a>    

            <div id="collapseTwo" class="panel-collapse collapse in">

                <div class="panel-body panel-body-border">

                    <?php print estate_listing_address($post->ID); ?>

                </div>

            </div>

        </div>

         

        <?php

        endwhile; // end of the loop

        $show_compare=1;

        ?>

        

        

        

      

        <?php     get_template_part ('/templates/listing_reviews'); ?>



    

        

       

        

    

        

        </div><!-- end single content -->

        

        <div class="property_page_container"> 

            <h3 class="panel-title" id="on_the_map"><?php esc_html_e('On the Map','wpestate');?></h3>

            <div class="google_map_on_list_wrapper">            

                <div id="gmapzoomplus"></div>

                <div id="gmapzoomminus"></div>

                <div id="gmapstreet"></div>



                <div id="google_map_on_list" 

                    data-cur_lat="<?php   echo $gmap_lat;?>" 

                    data-cur_long="<?php echo $gmap_long ?>" 

                    data-post_id="<?php echo $post->ID; ?>">

                </div>

            </div>    

        </div> 

 

            

        <?php   

        $show_sim_two=1;

        get_template_part ('/templates/similar_listings');
        $categoria = get_the_terms($edit_id, 'property_category');
        ?> 

    </div><!-- end 8col container-->

    

    

    

  

    

    <div class="clearfix visible-xs"></div>

    <div class=" 

        <?php

        if($options['sidebar_class']=='' || $options['sidebar_class']=='none' ){

            print ' col-md-4 '; 

        }else{

            print $options['sidebar_class'];

        }

        ?> 

        widget-area-sidebar listingsidebar2 listing_type_1" id="primary" >

        

            <div class="listing_main_image_price">
            <?php if($categoria[0]->term_id == '34'): ?>
                <?php  wpestate_show_price($post->ID,$currency,$where_currency,0); echo ' '.esc_html__( 'por dia','wpestate'); ?>

                <br>Mais de uma noite? <input type="checkbox" id="chkMoreNights" name="chkMoreNights" title="Mais de uma noite" />
            <?php else:?>
                <?php  wpestate_show_price($post->ID,$currency,$where_currency,0); echo ' '.esc_html__( 'por passagem','wpestate'); ?>
            <?php endif; ?>
        
            </div>

            <div class="booking_form_request" id="booking_form_request">

            <h3 ><?php esc_html_e('Book Now','wpestate');?></h3>

             

                <div class="has_calendar calendar_icon">

                    <input type="text" id="start_date" placeholder="<?php esc_html_e('Check in','wpestate'); ?>"  class="form-control calendar_icon" size="40" name="start_date" value="">

                </div>

                <?php
                    if($categoria[0]->term_id == '34'):
                ?>

                <div class=" has_calendar calendar_icon" id="div_end_date">

                    <input type="text" id="end_date" disabled placeholder="<?php esc_html_e('Check Out','wpestate'); ?>" class="form-control calendar_icon" size="40" name="end_date" value="">

                </div>

                <?php
                    endif;
                ?>

                <div class=" has_calendar guest_icon ">
                    <div class="dropdown custom_select form-control">
                    <?php 
$value = 0;
$i=0;
$custom_fields = get_option( 'wp_estate_custom_fields', true); 
if( !empty($custom_fields)){  
   $name =   $custom_fields[$i][0];
   $label=   $custom_fields[$i][1];
   $type =   $custom_fields[$i][2];
   $slug         =   wpestate_limit45(sanitize_title( $name ));
   $slug         =   sanitize_key($slug);
   $value=esc_html(get_post_meta($post_id, $slug, true));
}



//	echo 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'.$value.$property_category.'xxxxxxxxxxxxxx';	  
if ($property_category == 'passagens') {  
                    //$max_guest = $value;                   
}

$my_prop_selected = get_the_terms($edit_id, 'property_category');
$categoria = $my_prop_selected[0]->name;


switch ($categoria) {
    case 'passagens':
        $capacidade = get_post_meta($post_id, 'passagens', true);
    break;
    
    case 'aluguel':
        $capacidade = get_post_meta($post_id, 'capacidade', true);
    break;
}
                    
                    echo '

                    

                        <div data-toggle="dropdown" id="booking_guest_no_wrapper" class="filter_menu_trigger" data-value="all">Nº de Pessoas
                            <span class="caret caret_filter"></span>

                        </div>           

                        <input type="hidden" name="booking_guest_no"  value="">

                        <ul  class="dropdown-menu filter_menu booking_guest_no_wrapper" role="menu" aria-labelledby="booking_guest_no_wrapper">
                            ';
                        for($i=0; $i <= $capacidade; $i++){

                            if($i > 1){
                                echo '<li role="presentation" data-value="'.$i.'" value="'.$i.'">'.$i.' pessoas</li>';
                            }
                            elseif($i == 1) {
                                echo '<li role="presentation" data-value="'.$i.'" value="'.$i.'">'.$i.' pessoa</li>';
                            }
                        }
                        
                    echo '</ul>        

                    ';

                    ?> 
                    </div>
                </div>           

<?php
require get_template_directory().'/Pagseguro.php';
$pagseguro = new Pagseguro();

global $current_user;
$userID             =   $current_user->ID;
$dono_id = wpsestate_get_author($post_id);
$dono_van   = get_userdata($dono_id);

$first_name             =   get_the_author_meta( 'first_name' , $userID );
$last_name              =   get_the_author_meta( 'last_name' , $userID );
$user_email             =   get_the_author_meta( 'user_email' , $userID );
$user_mobile            =   get_the_author_meta( 'mobile' , $userID );
$user_phone             =   get_the_author_meta( 'phone' , $userID );
$description            =   get_the_author_meta( 'description' , $userID );
$facebook               =   get_the_author_meta( 'facebook' , $userID );
$twitter                =   get_the_author_meta( 'twitter' , $userID );
$linkedin               =   get_the_author_meta( 'linkedin' , $userID );
$pinterest              =   get_the_author_meta( 'pinterest' , $userID );
$user_skype             =   get_the_author_meta( 'skype' , $userID );

// Novos campos adicionados
$cpf                    =   get_the_author_meta( 'cpf' , $userID );
$banco                  =   get_the_author_meta( 'banco' , $userID );
$agencia                =   get_the_author_meta( 'agencia' , $userID );
$conta                  =   get_the_author_meta( 'conta' , $userID );
$titular                =   get_the_author_meta( 'titular' , $userID );
$email_pagseguro        =   get_the_author_meta( 'email_pagseguro' , $userID );

$user_title             =   get_the_author_meta( 'title' , $userID );
$user_custom_picture    =   get_the_author_meta( 'custom_picture' , $userID );
$user_small_picture     =   get_the_author_meta( 'small_custom_picture' , $userID );
$image_id               =   get_the_author_meta( 'small_custom_picture',$userID); 
$about_me               =   get_the_author_meta( 'description' , $userID );
$live_in                =   get_the_author_meta( 'live_in' , $userID );
$i_speak                =   get_the_author_meta( 'i_speak' , $userID );

?>

<script type="text/javascript" src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>

                <p class="full_form " id="add_costs_here"></p>            

                <div class="div_select_pagamento" style="display:none;">
                    <h3>Forma de Pagamento</h3>
                    <input type="hidden" id="sessionId" name="sessionId" value="<?php echo $pagseguro->sessionId?>" />
                    <input type="hidden" id="senderHash" name="senderHash" value="" />
                    <input type="hidden" id="cardToken" name="cardToken" value="" />

                    <input type="hidden" id="van_id" name="van_id" value="<?php echo $post_id?>" />
                    <input type="hidden" id="dono_id" name="dono_id" value="<?php echo $dono_id?>" />
                    <input type="hidden" id="comprador_nome" name="comprador_nome" value="<?php echo $first_name?>" />
                    <input type="hidden" id="comprador_sobrenome" name="comprador_sobrenome" value="<?php echo $last_name?>" />
                    <input type="hidden" id="comprador_email" name="comprador_email" value="<?php echo $user_email?>" />
                    <input type="hidden" id="comprador_telefone" name="comprador_telefone" value="<?php echo $user_mobile?>" />
                    <input type="hidden" id="comprador_nascimento" name="comprador_nascimento" value="<?php echo $user_phone?>" />
                    <input type="hidden" id="comprador_cpf" name="comprador_cpf" value="<?php echo $cpf?>" />

                    <input type="hidden" id="forma_pagamento" name="forma_pagamento" value="" />

                    <div class="radio">
                        <label>
                            <input type="radio" name="formaPagamento" id="pagBoleto" value="boleto">
                            Boleto Bancário
                        </label>
                    </div>

                    <div class="radio">
                        <label>
                            <input type="radio" name="formaPagamento" id="pagCartao" value="cartao" checked>
                            Cartão de Crédito
                        </label>
                    </div>

                    <div class="radio">
                        <label>
                            <input type="radio" name="formaPagamento" id="pagDebito" value="debito">
                            Débito
                        </label>
                    </div>

                    <div class="pagamento_cartao">
                        <!--<input type="text" id="titular_cartao" placeholder="Nome do Titular" class="form-control"  name="titular_cartao" value="">-->
                        <div class="bandeira_cartao">
                            <img src="" />
                        </div>

                        <input type="hidden" name="bandeira_cartao" id="bandeira_cartao">
                        <input type="text" id="numero_cartao" maxlength="16" placeholder="Nº do Cartão" class="form-control valida_pagseguro"  name="numero_cartao" value="">

                        <div class="col-md-8" style="padding-left:0px;">
                            <div class="col-md-6" style="padding:0px">
                                <input type="text" id="validade_mes" placeholder="Valid. MM" class="form-control valida_pagseguro"  name="validade_mes" value="">
                            </div>
                            <div class="col-md-6" style="padding:0px">
                                <input type="text" id="validade_ano" placeholder="AAAA" class="form-control valida_pagseguro"  name="validade_ano" value="">
                            </div>
                        </div>

                        <div class="col-md-4" style="padding:0px;">
                            <input type="text" id="cvv_cartao" placeholder="CVV" class="form-control valida_pagseguro"  name="cvv_cartao" value="">
                        </div>
                    </div>

                    <div class="pagamento_debito" style="display:none;">
                        <p>Selecione o seu Banco</p>

                        <input type="hidden" id="pag_banco" name="pag_banco" value="">

                        <div class="col-md-3">
                            <a data-id="BRADESCO" class="thumbnail">
                                <img src="https://stc.pagseguro.uol.com.br/public/img/payment-methods-flags/68x30/bradesco.png" />
                            </a>
                        </div>

                        <div class="col-md-3">
                            <a data-id="ITAU" class="thumbnail">
                                <img src="https://stc.pagseguro.uol.com.br/public/img/payment-methods-flags/68x30/itau.png" />
                            </a>
                        </div>

                        <div class="col-md-3">
                            <a data-id="BANCO_BRASIL" class="thumbnail">
                                <img src="https://stc.pagseguro.uol.com.br/public/img/payment-methods-flags/68x30/bb.png" />
                            </a>
                        </div>

                        <div class="col-md-3">
                            <a data-id="HSBC" class="thumbnail">
                                <img src="https://stc.pagseguro.uol.com.br/public/img/payment-methods-flags/68x30/hsbc.png" />
                            </a>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="erro_pagseguro alert alert-danger" style="display:none;"></div>
                <div class="loading_msg alert alert-warning" style="display:none;">
                    Carregando...
                </div>
                <div id="booking_form_request_mess" class="alert alert-message"></div>


                <input type="hidden" id="listing_edit" name="listing_edit" value="<?php echo $post_id;?>" />



                <div class="submit_booking_front_wrapper">

                    <?php   

                    $overload_guest                 =   floatval   ( get_post_meta($post_id, 'overload_guest', true) );

                    $price_per_guest_from_one       =   floatval   ( get_post_meta($post_id, 'price_per_guest_from_one', true) );

                    ?>

                    <input type="submit" id="submit_booking_front" data-maxguest="<?php echo $max_guest; ?>" data-overload="<?php echo $overload_guest;?>" data-guestfromone="<?php echo $price_per_guest_from_one; ?>"  class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button" value="<?php

                        switch ($categoria) {
                            case 'aluguel':
                                echo "Faça sua Reserva";
                            break;
                            
                            case 'passagens':
                                echo "Comprar";
                            break;
                        }
                    ?>" />

                    <?php wp_nonce_field( 'booking_ajax_nonce', 'security-register-booking_front' );?>

                </div>



                <div class="third-form-wrapper">

                    <?php if($categoria == "aluguel"):?>
                    <div class="col-md-6 reservation_buttons">
                        <div id="add_favorites" class=" <?php print $favorite_class;?>" data-postid="<?php the_ID();?>">
                            <?php echo $favorite_text;?>
                        </div>                 
                    </div>

                    <div class="col-md-6 reservation_buttons">
                        <div id="contact_host" class="col-md-6"  data-postid="<?php the_ID();?>">
                            <?php esc_html_e('Contact Owner','wpestate');?>
                        </div>  
                    </div>

                    <?php else:?>
                    <div class="col-md-6 col-md-offset-3 reservation_buttons text-center">
                        <div id="add_favorites" class=" <?php print $favorite_class;?>" data-postid="<?php the_ID();?>">
                            <?php echo $favorite_text;?>
                        </div>                 
                    </div>
                    <?php endif?>

                </div>

                

                <?php 

                if (has_post_thumbnail()){

                    $pinterest = wp_get_attachment_image_src(get_post_thumbnail_id(),'wpestate_property_full_map');

                }

                ?>



                <div class="prop_social">

                    <span class="prop_social_share"><?php esc_html_e('Share','wpestate');?></span>

                    <a href="http://www.facebook.com/sharer.php?u=<?php echo esc_url(get_permalink()); ?>&amp;t=<?php echo urlencode(get_the_title()); ?>" target="_blank" class="share_facebook"><i class="fa fa-facebook fa-2"></i></a>

                    <a href="http://twitter.com/home?status=<?php echo urlencode(get_the_title() .' '.esc_url( get_permalink()) ); ?>" class="share_tweet" target="_blank"><i class="fa fa-twitter fa-2"></i></a>

                    <a href="https://plus.google.com/share?url=<?php echo esc_url(get_permalink()); ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" target="_blank" class="share_google"><i class="fa fa-google-plus fa-2"></i></a> 

                    <?php if (isset($pinterest[0])){ ?>

                        <a href="http://instagram.com/?url=<?php echo esc_url(get_permalink()); ?>&amp;media=<?php echo $pinterest[0];?>&amp;description=<?php echo urlencode(get_the_title()); ?>" target="_blank" class="share_pinterest"> <i class="fa fa-instagram fa-2"></i> </a>      

                    <?php } ?>           

                </div>             



        </div>

        
<!--
        <div class="owner_area_wrapper_sidebar">

            <?php get_template_part ('/templates/owner_area2'); ?>

        </div>
-->
        

        <?php  include(locate_template('sidebar-listing.php')); ?>

    </div>

</div>   



<?php get_footer(); ?>