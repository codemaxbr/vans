<?php
global $unit;
global $edit_id;
global $property_size;
global $property_lot_size;
global $property_rooms;
global $property_bedrooms;
global $property_bathrooms;
global $custom_fields;    
global $custom_fields_array;

$measure_sys            = esc_html ( get_option('wp_estate_measure_sys','') ); 
?> 



<div class="col-md-12">
    <div class="user_dashboard_panel">
    <h4 class="user_dashboard_panel_title"><?php esc_html_e('Listing Details','wpestate');?></h4>

    <div class="col-md-12" id="profile_message"></div>
              
    <div class="col-md-4">
        <p>
            <label for="property_size"> <?php esc_html_e('Tamanho em','wpestate');print ' '.$measure_sys.'<sup>2</sup>';?></label>
            <input type="text" id="property_size" size="40" class="form-control"  name="property_size" value="<?php print $property_size;?>">
        </p>
    </div>
    
    <!-- campos customizados movidos para cá -->
    
     <!-- Add custom details -->

    <?php
     
    $i=0;
    if( !empty($custom_fields)){  
        while($i< count($custom_fields)  ){
            $name  =   $custom_fields[$i][0];
            $label =   $custom_fields[$i][1];
            $type  =   $custom_fields[$i][2];
            $slug  =   str_replace(' ','_',$name);

            $slug         =   wpestate_limit45(sanitize_title( $name ));
            $slug         =   sanitize_key($slug);

            $i++;

            if (function_exists('icl_translate') ){
                $label     =   icl_translate('wpestate','wp_estate_property_custom_front_'.$label, $label ) ;
            }   

            print '<div class="col-md-4"><p><label for="'.$slug.'">'.$label.'</label>';

            if ($type=='long text'){
                 print '<textarea type="text" class="form-control"  id="'.$slug.'"  size="40" name="'.$slug.'" rows="3" cols="42">'.$custom_fields_array[$slug].'</textarea>';
            }else{
                 print '<input type="text" class="form-control"  id="'.$slug.'" size="40" name="'.$slug.'" rows="3" cols="42" value="'.$custom_fields_array[$slug].'">';
            }
            print '</p>  </div>';

            if ($type=='date'){
                print '<script type="text/javascript">
                    //<![CDATA[
                    jQuery(document).ready(function(){
                        '.wpestate_date_picker_translation($slug).'
                    });
                    //]]>
                    </script>';
            }
        }
    }

    ?>
    
   
    
     <div class="col-md-4">
        <p>
            <label for="property_rooms"><?php esc_html_e('Quartos','wpestate');?></label>
            <input type="text" id="property_rooms" size="40" class="form-control"  name="property_rooms" value="<?php print $property_rooms;?>">
        </p>
    </div>
    
    <!-- tipo de cancelamento -->
    <div class="col-md-4">
        <p>
            <label for="property_rooms"><?php esc_html_e('Tipo de Cancelamento','wpestate');?></label><br>
			<label>
			  <input type="radio" name="tipo_cancelamento" value="Flexível" id="tipo_cancelamento_0">
			  Flexível</label>
			
			<label>
			  <input type="radio" name="tipo_cancelamento" value="Rígido" id="tipo_cancelamento_1">
			  Rígido</label>
			<br>
		<!-- <input type="text" id="tipo_cancelamento" size="40" class="form-control"  name="tipo_cancelamento" value="<?php //print $tipo_cancelamento;?>">-->
        </p>
    </div>
    <!-- /tipo de cancelamento -->
    
     <div class="col-md-4">
        <p>
            <label for="property_roteiros"><?php esc_html_e('Roteiros','wpestate');?></label>
            <!-- input type="text" id="property_roteiros" size="40" class="form-control"  name="property_roteiros" value="<?php //print $property_roteiros;?>"-->
            
            <textarea type="text" class="form-control"  id="property_roteiros"  size="80" name="property_roteiros" rows="10" cols="82"></textarea>
            
        </p>
    </div>
    
        <!-- gallery image -->
     <div class="col-md-4">
        <p>
            <?php echo [EasyGallery id='1'] ;?>
            
        </p>
    </div>
    <!--/gallery image -->
    
    <!-- slider -->
    <div class="col-md-4">
        <p>
            <?php //putRevSlider('', 'property_details'); ?>
        </p>
    </div>
    
 
    
 
   <!-- campos customizados movidos daqui -->
  
    
    <div class="col-md-12" style="display: inline-block;">  
        <input type="hidden" name="" id="listing_edit" value="<?php echo $edit_id;?>">
        <input type="submit" class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button" id="edit_prop_details" value="<?php esc_html_e('Save', 'wpestate') ?>" />
    </div>

<style>.continua {
    max-width: 200px;
    font-size: 16px;
    margin-top: -64px;
    width: 100%;
    color: #fff;
    font-size: 18px;
    line-height: 18px;
    font-weight: 600;
    background-color: #b881fc;
    border: none;
    padding: 13px 30px;
    border-radius: 2px;
    float: right;
}
</style>
    <div class="col-md-12" style="display: inline-block;"> 
        <input type="button" class="continua" id="goto_2" value="<?php esc_html_e('Proxima', 'wpestate') ?>" onclick='location.href="http://www.riodejaneiro.org.br/vans/edit-listing/?listing_edit=<?php echo $edit_id;?>&action=location"' />
    </div>
  

</div>  
