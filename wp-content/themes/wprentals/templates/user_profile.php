<?php

global $current_user;

get_currentuserinfo();

$userID                 =   $current_user->ID;
$user_login             =   $current_user->user_login;
$first_name             =   get_the_author_meta( 'first_name' , $userID );
$last_name              =   get_the_author_meta( 'last_name' , $userID );
$user_email             =   get_the_author_meta( 'user_email' , $userID );
$user_mobile            =   get_the_author_meta( 'mobile' , $userID );
$user_phone             =   get_the_author_meta( 'phone' , $userID );
$description            =   get_the_author_meta( 'description' , $userID );
$facebook               =   get_the_author_meta( 'facebook' , $userID );
$twitter                =   get_the_author_meta( 'twitter' , $userID );
$linkedin               =   get_the_author_meta( 'linkedin' , $userID );
$pinterest              =   get_the_author_meta( 'pinterest' , $userID );
$user_skype             =   get_the_author_meta( 'skype' , $userID );

// Novos campos adicionados
$cpf                    =   get_the_author_meta( 'cpf' , $userID );
$banco                  =   get_the_author_meta( 'banco' , $userID );
$agencia                =   get_the_author_meta( 'agencia' , $userID );
$conta                  =   get_the_author_meta( 'conta' , $userID );
$titular                =   get_the_author_meta( 'titular' , $userID );
$email_pagseguro        =   get_the_author_meta( 'email_pagseguro' , $userID );

$user_title             =   get_the_author_meta( 'title' , $userID );
$user_custom_picture    =   get_the_author_meta( 'custom_picture' , $userID );
$user_small_picture     =   get_the_author_meta( 'small_custom_picture' , $userID );
$image_id               =   get_the_author_meta( 'small_custom_picture',$userID); 
$about_me               =   get_the_author_meta( 'description' , $userID );
$live_in                =   get_the_author_meta( 'live_in' , $userID );
$i_speak                =   get_the_author_meta( 'i_speak' , $userID );

if($user_custom_picture==''){
    $user_custom_picture=get_template_directory_uri().'/img/default_user.png';
}

$faltamDados = 0;
if (empty($first_name) ||
	empty($last_name) ||
	empty($user_email) ||
	empty($user_mobile) ||
	empty($user_phone) ||
	empty($cpf) ||
	empty($banco) ||
	empty($agencia) ||
    empty($titular) ||
    empty($conta) ||
    empty($email_pagseguro) ||
	empty($about_me) ) {
	$faltamDados = 1;
}






?>





<div class="user_profile_div">      
    <?php if ($faltamDados == 1):?>
    <div class="col-md-12">
        <div class="well" style="overflow:auto;">
            <div class="col-md-12" style="padding:0px">
                É importane que todos os campos estejam preenchidos corretamente
            </div>
        </div>
    </div>
    <?php endif;?>

        

    <div class=" row">
        <div class="col-md-12">

            <div class="user_dashboard_panel">
                <h4 class="user_dashboard_panel_title">
                    Meu Cadastro
                </h4>

                <div class="col-md-4">

                    <p>
                        <label for="firstname">Nome</label>
                        <input type="text" id="firstname" class="form-control" value="<?php echo $first_name;?>"  name="firstname">
                    </p>

                    <p>
                        <label for="secondname">Sobrenome</label>
                        <input type="text" id="secondname" class="form-control" value="<?php echo $last_name;?>"  name="firstname">
                    </p>

                    <p>
                        <label for="useremail"><?php esc_html_e('Email','wpestate');?></label>
                        <input type="text" id="useremail"  class="form-control" value="<?php echo $user_email;?>"  name="useremail">
                    </p>
                </div>                                   

                <div class="col-md-4">

                    <p>
                        <label for="userphone">Data de Nascimento</label>
                        <input type="text" id="userphone" class="form-control" value="<?php echo $user_phone;?>"  name="userphone">
                    </p>

                    <p>
                        <label for="usermobile">Celular</label>
                        <input type="text" id="usermobile" class="form-control" value="<?php echo $user_mobile;?>"  name="usermobile">
                    </p>

                    <p>
                        <label for="cpf">CPF</label>
                        <input type="text" id="cpf" class="form-control" value="<?php echo $cpf;?>"  name="cpf">
                    </p>
                </div>

                <div class="col-md-4">
                    <div  id="profile-div">
                        <?php print '<img id="profile-image" src="'.$user_custom_picture.'" alt="user image" data-profileurl="'.$user_custom_picture.'" data-smallprofileurl="'.$image_id.'" >';?>
                        <div id="upload-container">                 
                            <div id="aaiu-upload-container">                 
                                <button id="aaiu-uploader" class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button"><?php esc_html_e('Upload Image','wpestate');?></button>
                                <div id="aaiu-upload-imagelist">
                                    <ul id="aaiu-ul-list" class="aaiu-upload-list"></ul>
                                </div>
                            </div>  
                        </div>

                        <span class="upload_explain"><?php esc_html_e('* recommended size: minimum 550px ','wpestate');?></span>
                    </div>
                </div>

                <!--
                <div class="col-md-4">
                    <p>
                        <label for="userlinkedin"><?php esc_html_e('Cidade *','wpestate');?></label>
                        <input type="text" id="userlinkedin" class="form-control"  value="<?php echo $linkedin;?>"  name="userlinkedin">
                    </p>

                     <p>
                        <label for="userpinterest"><?php esc_html_e('CPF/CNPJ *','wpestate');?></label>
                        <input type="text" id="userpinterest" class="form-control"  height="100" value="<?php echo $pinterest;?>"  name="userpinterest">
                    </p>
                    
                    <p>
                        <label for="userpinterest"><?php esc_html_e('Cta Corrente N&deg; *','wpestate');?></label>
                        <input type="text" id="userpinterest" class="form-control"  height="100" value="<?php echo $pinterest;?>"  name="userpinterest">
                    </p>
                </div>
                -->
            </div>

            <?php   wp_nonce_field( 'profile_ajax_nonce', 'security-profile' );   ?>

            <div class="col-md-8">
                <div class="col-md-12">
                    <p>
                        <label for="about_me">Sobre mim</label>
                        <textarea id="about_me" class="form-control about_me_profile" name="about_me"><?php echo $about_me;?></textarea>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="user_dashboard_panel">
                <div class="col-md-12">
                    <h4 class="">Dados Bancários <small>* campos obrigatórios</small></h4>
                </div>
                
                <div class="col-md-4">    
                    <p>
                        <label for="banco"><?php esc_html_e('Nome do Banco *','wpestate');?></label>
                        <input type="text" id="banco" required class="form-control" value="<?php echo $banco;?>"  name="banco">
                    </p>
                    
                    <p>
                        <label for="agencia"><?php esc_html_e('Agência N&deg; *','wpestate');?></label>
                        <input type="text" id="agencia"  class="form-control" value="<?php echo $agencia;?>"  name="agencia">
                    </p>
                </div>

                <div class="col-md-4">
                    <p>
                        <label for="titular"><?php esc_html_e('Titular *','wpestate');?></label>
                        <input type="text" id="titular"  class="form-control" value="<?php echo $titular;?>"  name="titular">
                    </p>

                    <p>
                        <label for="conta"><?php esc_html_e('Conta N&deg; *','wpestate');?></label>
                        <input type="text" id="conta"  class="form-control" value="<?php echo $conta;?>"  name="agencia">
                    </p>
                </div>
            </div>
        </div>
    </div>  
    
    <div class="row">  
        <div class="col-md-12">
            <div class="user_dashboard_panel">
                <h4 class="user_dashboard_panel_title">Dados do PagSeguro</h4>                

                <div class="col-md-8">
                    <p>
                        <label for="email_pagseguro"><?php esc_html_e('E-mail cadastrado no PagSeguro *','wpestate');?></label>
                        <input type="text" id="email_pagseguro" class="form-control" value="<?php echo $email_pagseguro;?>"  name="email_pagseguro">
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="col-md-3">
                <p class="fullp-button">
                    <button type="submit" class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button" id="update_profile">Salvar alterações</button>
                </p>
            </div>

            <div class="col-md-9">
                <div id="profile_message"></div>
            </div>
        </div>
    </div>
</div>
   
    <div class="user_dashboard_panel">
        <h4 class="user_dashboard_panel_title">Alterar Senha</h4>

        <div class="col-md-12" id="profile_pass"></div> 

        <p class="col-md-4">
           <label for="oldpass">Senha Anterior</label>
           <input  id="oldpass" value=""  class="form-control" name="oldpass" type="password">
        </p>

        <p class="col-md-4">
            <label for="newpass">Nova Senha</label>
            <input  id="newpass" value="" class="form-control" name="newpass" type="password">
        </p>

        <p class="col-md-4">
            <label for="renewpass">Confirme a Nova Senha</label>
            <input id="renewpass" value=""  class="form-control" name="renewpass" type="password">
        </p>

        <?php   wp_nonce_field( 'pass_ajax_nonce', 'security-pass' );   ?>
        <p class="fullp-button">
           <button class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button" id="change_pass">Mudar Senha</button>
        </p>
    </div>
 </div>